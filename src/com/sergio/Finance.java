package com.sergio;

public class Finance {

    Bank[] banks;

    public Finance(Bank[] banks) {
        this.banks = banks;
    }

    public  Bank getBank(String bankName) {

        for (int i = 0; i < banks.length; i++) {
            if ((banks[i].name.toLowerCase().contains(bankName.toLowerCase()))) {
                return banks[i];
            }
        }

        return null;
    }



    public boolean hasBank(String bankName) {


        for (int i = 0; i < banks.length; i++) {
            if ((banks[i].name.toLowerCase().contains(bankName.toLowerCase()))) {
                return true;
            }
        }
        return false;
    }


    public void printAllBanks() {

        for (int i = 0; i < banks.length; i++) {
            banks[i].printCurrency();
        }
        System.out.println("");
    }



}
