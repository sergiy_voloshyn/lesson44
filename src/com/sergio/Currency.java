package com.sergio;

import static com.sergio.Bank.*;


public class Currency {

    String usd;
    float buyUsd;
    float sellUsd;

    String eur;
    float buyEur;
    float sellEur;

    String rub;
    float buyRub;
    float sellRub;

    public Currency(String usd, float buyUsd, float saleUsd, String eur, float buyEur, float saleEur, String rub, float buyRub, float saleRub) {
        this.usd = usd;
        this.buyUsd = buyUsd;
        this.sellUsd = saleUsd;
        this.eur = eur;
        this.buyEur = buyEur;
        this.sellEur = saleEur;
        this.rub = rub;
        this.buyRub = buyRub;
        this.sellRub = saleRub;
    }

    public float getSellRate(String currency) {
        switch (currency) {
            case USD:
                return this.sellUsd;
            case EUR:
                return this.sellEur;
            case RUB:
                return this.sellRub;
            default:
                return this.sellUsd;
        }


    }

    public float getBuyRate(String currency) {
        switch (currency) {
            case USD:
                return this.buyUsd;
            case EUR:
                return this.buyEur;
            case RUB:
                return this.buyRub;
            default:
                return this.buyUsd;
        }

    }

    void printCurrencyRate(String currency) {

        switch (currency) {
            case USD:
                System.out.println("Currency " + USD + " (buy\\sale):" + this.getBuyRate(USD) + "\\" + this.getSellRate(USD));
                break;
            case EUR:
                System.out.println("Currency " + EUR + " (buy\\sale):" + this.getBuyRate(EUR) + "\\" + this.getSellRate(EUR));
                break;
            case RUB:
                System.out.println("Currency " + RUB + " (buy\\sale):" + this.getBuyRate(RUB) + "\\" + this.getSellRate(RUB));
                break;
            default:
                System.out.println("Currency " + USD + " (buy\\sale):" + this.getBuyRate(USD) + "\\" + this.getSellRate(USD));
        }




    }

}


