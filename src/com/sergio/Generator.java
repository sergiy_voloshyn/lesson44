package com.sergio;


import static com.sergio.Bank.USD;
import static com.sergio.Bank.EUR;
import static com.sergio.Bank.RUB;

import com.sergio.Currency;

public final class Generator {

    private Generator() {
    }

    public static Bank[] generateBanks() {
        Bank[] banks = new Bank[3];

        banks[0] = new Bank(
         new Currency(USD, 27.00F, 27.17F,EUR, 30.100F, 31.17F,RUB, 0.47F, 0.49F)
         , "Privat");

        banks[1] = new Bank(
                new Currency(USD, 27.20F, 27.37F,EUR, 30.200F, 31.47F,RUB, 0.45F, 0.48F)
                , "BNP");

        banks[2] = new Bank(
                new Currency(USD, 27.30F, 27.47F,EUR, 30.300F, 31.57F,RUB, 0.48F, 0.49F)
                , "Mega");


        return banks;
    }
}


