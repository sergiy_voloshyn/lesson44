package com.sergio;

import com.sergio.Bank;

import java.util.Scanner;

import static com.sergio.Bank.*;
import static com.sergio.Generator.generateBanks;


public class Main {
    /*
    2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар или евро по одному из заранее
     известных (значения задаются в переменных/константах/моделях) курсов (например, курс приватБанка, ОщадБанка,
      и банка ПУМБ). Название банка вводится с консоли.

    2.3** На её основе сделать программу для конвертации трёх валют в трёх разных банках по двум курсам: покупка и
     продажа. Возможные курсы валют и имена банков известны заранее. Название банка, валюты и операции (покупка/продажа)
      для конвертации вводятся с консоли.
     */





    public static void main(String[] args) {
        // write your code here

        Bank[] banks = generateBanks();
        Finance finance = new Finance(banks);
        finance.printAllBanks();

        Scanner scan = new Scanner(System.in);

        System.out.println("Please, input bank name: ");
        String inputBankName = scan.nextLine().toLowerCase();

        System.out.println("Please, input operation name: " + BUY + " or " + SALE);
        String inputOperationName = scan.nextLine().toUpperCase();

        System.out.println("Please, input currency: " + USD + " - " + EUR + " - " + RUB);
        String inputCurrency = scan.nextLine().toUpperCase();


        System.out.println("Please, input amount of money: ");
        float inputUAH = Integer.parseInt(scan.nextLine());

        //check bank
        if (finance.hasBank(inputBankName)) {
            //check 0
            if (inputUAH > 0) {
                Bank bankInput = finance.getBank(inputBankName);
                bankInput.printConvertedCurrency(inputUAH, inputCurrency, inputOperationName);

            } else {
                System.out.println("Incorrect amount of money!");
            }
        } else {
            System.out.println("Incorrect bank name!");
        }


    }
}
/*

Для реализации задания 2.1 можно создать класс Банк, который в полях класса будет хранить курсы валют и иметь несколько
методов для конвертации гривны. Названия валют (USD, EUR) можно сделать константами (static final).
Класс Main будет содержать массив банков, считывать данные от пользователя и в выбранном банке конвертировать валюту.
Класс Generator возвращает массив банков, с уже забитыми курсами валют.

 */
/*
 /**
     * This program convert UAN to USD or EUR

public static void main(String[] args) {

    //init variables
    String USD = "USD";
    String EUR = "EUR";

    float COURSE_USD = 24.5f;
    float COURSE_EUR = 26f;

    //init scanner
    Scanner scan = new Scanner(System.in);

    //enter amount of money
    System.out.println("Enter the amount of money that you want to change: ");
    int amount = Integer.parseInt(scan.nextLine());

    //enter currency
    System.out.println("Enter the currency to convert (USD or EUR): ");
    String currency = scan.nextLine();

    //convert uan to user currency
    if (USD.equalsIgnoreCase(currency)) {
        System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD));
    } else if (EUR.equalsIgnoreCase(currency)) {
        System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / COURSE_EUR));
    } else {
        System.err.println("Can't convert to " + currency);
    }

}

 */