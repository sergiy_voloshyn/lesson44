package com.sergio;

public class Bank {

    static final String USD = "USD";
    static final String EUR = "EUR";
    static final String RUB = "RUB";
    static final String UAH = "UAH";


    static final String SALE = "SALE";
    static final String BUY = "BUY";


    /*float buyCurrencyUSD;
    float saleCurrencyUSD;
    float buyCurrencyEUR;
    float saleCurrencyEUR;
    float buyCurrencyRUB;
    float saleCurrencyRUB;*/

    Currency currency;
    String name;

    public Bank(Currency currency, String name) {
        this.currency = currency;
        this.name = name;
    }

    public void printBankName() {
        System.out.println(this.name);
    }

    public float getCurrencySellRate(String currency) {

        switch (currency) {
            case USD:
                return this.currency.getSellRate(USD);
            case EUR:
                return this.currency.getSellRate(EUR);
            case RUB:
                return this.currency.getSellRate(RUB);
        }
        return 0;
    }

    public float getCurrencyBuyRate(String currency) {

        switch (currency) {
            case USD:
                return this.currency.getBuyRate(USD);
            case EUR:
                return this.currency.getBuyRate(EUR);
            case RUB:
                return this.currency.getBuyRate(RUB);
        }
        return 0;
    }

    public void printCurrency() {
        System.out.println("Bank:" + this.name);
        this.currency.printCurrencyRate(USD);
        this.currency.printCurrencyRate(EUR);
        this.currency.printCurrencyRate(RUB);


    }


    public void printConvertedCurrency(float money, String currency, String operation) {

        exit:
        switch (operation) {
            case BUY:
                switch (currency.toUpperCase()) {
                    case USD:
                        System.out.println(String.format("%.2f %S", money / this.currency.getBuyRate(USD), USD));

                        break exit;
                    case EUR:
                        System.out.println(String.format("%.2f %S", money / this.currency.getBuyRate(EUR), EUR));

                        break exit;
                    case RUB:
                        System.out.println(String.format("%.2f %S", money / this.currency.getBuyRate(RUB), RUB));

                        break exit;
                    default:
                        System.out.println(String.format("%.2f %S", money / this.currency.getBuyRate(USD), USD));

                        break exit;
                }
            case SALE:
                switch (currency.toUpperCase()) {
                    case USD:
                        System.out.println(String.format("%.2f %S", money * this.currency.getSellRate(USD), UAH));
                        break exit;
                    case EUR:
                        System.out.println(String.format("%.2f %S", money * this.currency.getSellRate(EUR), UAH));

                        break exit;
                    case RUB:
                        System.out.println(String.format("%.2f %S", money * this.currency.getSellRate(RUB), UAH));

                        break exit;
                    default:
                        System.out.println(String.format("%.2f %S", money * this.currency.getSellRate(USD), UAH));
                        break exit;

                }

        }
    }


}
